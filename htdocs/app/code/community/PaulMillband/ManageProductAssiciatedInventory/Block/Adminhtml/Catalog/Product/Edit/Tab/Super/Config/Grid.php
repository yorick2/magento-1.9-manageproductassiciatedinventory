<?php
class PaulMillband_ManageProductAssiciatedInventory_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Config_Grid extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Config_Grid {

      /**
     * Prepare collection
     *
     * @return Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Config_Grid
     */
    protected function _prepareCollection($collection)
    {
        $allowProductTypes = array();
        foreach (Mage::helper('catalog/product_configuration')->getConfigurableAllowedTypes() as $type) {
            $allowProductTypes[] = $type->getName();
        }

        $product = $this->_getProduct();
        
        if(!isset($collection)){
            $collection = $product->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('sku')
                ->addAttributeToSelect('attribute_set_id')
                ->addAttributeToSelect('type_id')
                ->addAttributeToSelect('price')
              //  ->addAttributeToSelect('qty')
                ->addFieldToFilter('attribute_set_id',$product->getAttributeSetId())
                ->addFieldToFilter('type_id', $allowProductTypes)
                ->addFilterByRequiredOptions()
                ->joinAttribute('name', 'catalog_product/name', 'entity_id', null, 'inner');
        }

        $collection->joinField('qty',
            'cataloginventory/stock_item',
            'qty',
            'product_id=entity_id',
            '{{table}}.stock_id=1',
            'left');
        
        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            Mage::getModel('cataloginventory/stock_item')->addCatalogInventoryToProductCollection($collection);
        }

        foreach ($product->getTypeInstance(true)->getUsedProductAttributes($product) as $attribute) {
            $collection->addAttributeToSelect($attribute->getAttributeCode());
            $collection->addAttributeToFilter($attribute->getAttributeCode(), array('notnull'=>1));
        }

        $this->setCollection($collection);

        if ($this->isReadonly()) {
            $collection->addFieldToFilter('entity_id', array('in' => $this->_getSelectedProducts()));
        }

        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns(){
        $this->addColumn('qty',
            array(
                'header'=> Mage::helper('catalog')->__('Qty'),
                'width' => '100px',
               // 'type'  => 'number',
                'index' => 'qty',
                'filter'    => false,
                'sortable'  => false,
                'renderer' => 'PaulMillband_ManageProductAssiciatedInventory_Block_Adminhtml_Catalog_Product_Edit_Tab_Super_Config_Renderer_Link'
        ));
           $this->addColumn('qty2', array(
        'header'    => Mage::helper('sales')->__('Stock Level'),
        'width'     => '20',
        'type'      => 'number',
        'index'     => 'qty2'
    ));
        return parent::_prepareColumns();    
    }
    
}